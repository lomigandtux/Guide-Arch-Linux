# Guide d'installation d'une Arch Linux

Mise en forme en LaTeX du guide Arch Linux de Frédéric Bézies
http://frederic.bezies.free.fr/blog/

## Compilation
Elle est faite avec xelatex. Les fichiers présentes dans le répertoire texmf doivent être
installés dans votre texmf personnel
Utiliser le script compils.sh dans le dossier latex

## Fontes utilisées
Elles sont dans le répertoire fonts. A installer dans votre répertoire .Fontes
